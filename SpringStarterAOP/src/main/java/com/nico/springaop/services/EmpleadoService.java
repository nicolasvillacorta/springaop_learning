package com.nico.springaop.services;

import org.springframework.stereotype.Service;

import com.nico.springaop.model.Empleado;

@Service
public class EmpleadoService {

	public Empleado crearEmpleado(String name, String empId) {
		Empleado emp = new Empleado();
		emp.setName(name);
		emp.setEmpId(empId);
		return emp;
	}
	
	public void removeEmpleado(String empId) {
		
	}
	
	
}
