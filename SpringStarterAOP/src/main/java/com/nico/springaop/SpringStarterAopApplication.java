package com.nico.springaop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringStarterAopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringStarterAopApplication.class, args);
	}

}
