package com.nico.springaop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nico.springaop.model.Empleado;
import com.nico.springaop.services.EmpleadoService;

@RestController
public class EmpleadosController {

	
	 @Autowired
	 private EmpleadoService empleadoService;
	 
	 @GetMapping("/add/empleado")
	 public Empleado addEmpleado(@RequestParam("name") String name, @RequestParam("empId") String empId) {
		 return empleadoService.crearEmpleado(name, empId);
	 }
	 
	 @GetMapping("/remove/empleado")
	 public String removeEmpleado(@RequestParam("empId") String empId) {
		 empleadoService.removeEmpleado(empId);
		 
		 return "Empleado removido!";
		 
	 }
	
}
