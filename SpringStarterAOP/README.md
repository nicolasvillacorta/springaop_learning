Los aspectos se usan para representar features transversales. 
En este ejemplo hice un aspecto con un Before y un After, que se ejecuta cada vez que se llama a un metodo.
Solamente lo hice con el createEmpleado, el otro no.

Esto fue lo que se imprimio cuando cree un empleado;
Before method: Empleado com.nico.springaop.services.EmpleadoService.crearEmpleado(String,String)
Creating Employee with name - Nicolas and id - 1
After method: Empleado com.nico.springaop.services.EmpleadoService.crearEmpleado(String,String)
Successfully created Employee with name - Nicolas and id - 1

El aspect tiene que ser un componente (las dos anotaciones). 
Tutorial seguido de esta pag: https://www.javainuse.com/spring/spring-boot-aop